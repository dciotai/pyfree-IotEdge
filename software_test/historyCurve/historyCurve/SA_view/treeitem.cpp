/****************************************************************************

****************************************************************************/
#include "treeitem.h"
// #include <QDebug>

TreeItem::TreeItem(const QVector<QVariant> &data, 
    TreeItem *parent)
: parentItem(parent)
, itemData(data) 
, m_checked(false)
, _state(0)
{
}

TreeItem::~TreeItem()
{
    qDeleteAll(childItems);
	parentItem=NULL;
}

TreeItem *TreeItem::child(int number)
{
    return childItems.value(number);
}

bool TreeItem::isEmpty()
{
	return childItems.empty();
}

int TreeItem::childCount() const
{
    return childItems.count();
}

int TreeItem::childNumber() const
{
    if (parentItem)
        return parentItem->childItems.indexOf(const_cast<TreeItem*>(this));

    return 0;
}

int TreeItem::columnCount() const
{
    return itemData.count();
}

QVariant TreeItem::data(int column) const
{
    return itemData.value(column);
}

bool TreeItem::insertChildren(int position, int count, int columns)
{
    if (position < 0 || position > childItems.size())
        return false;

    for (int row = 0; row < count; ++row) {
        QVector<QVariant> data(columns);
        TreeItem *item = new TreeItem(data, this);
        // if(0==row)
        //     item->setIcon();
        childItems.insert(position, item);
    }

    return true;
}

bool TreeItem::insertColumns(int position, int columns)
{
    if (position < 0 || position > itemData.size())
        return false;

    for (int column = 0; column < columns; ++column)
        itemData.insert(position, QVariant());

    foreach (TreeItem *child, childItems)
        child->insertColumns(position, columns);

    return true;
}

TreeItem *TreeItem::parent()
{
	if(NULL==parentItem)
		return NULL;
	return parentItem;
}

bool TreeItem::removeChildren(int position, int count)
{
    if (position < 0 || position + count > childItems.size())
        return false;

    for (int row = 0; row < count; ++row)
        delete childItems.takeAt(position);

    return true;
}

bool TreeItem::removeColumns(int position, int columns)
{
    if (position < 0 || position + columns > itemData.size())
        return false;

    for (int column = 0; column < columns; ++column)
        itemData.remove(position);

    foreach (TreeItem *one_child, childItems)
        one_child->removeColumns(position, columns);

    return true;
}

bool TreeItem::setData(int column, const QVariant &value)
{
    if (column < 0 || column >= itemData.size())
        return false;

    itemData[column] = value;
    return true;
}

bool TreeItem::contains ( const QVariant & treeValue ) const
{
    foreach (TreeItem *one_child, childItems)
    {
        if (one_child->data(0).toString()==treeValue.toString())
        {
            return true;
        }
    }
    return false;
}

int TreeItem::indexChild(const QVariant & treeValue) const
{
    for (int i = 0; i < childItems.size(); i++)
    {
        if(childItems[i]->data(0).toString()==treeValue.toString()){
            return i;
        }
    }

    return -1;
}

void TreeItem::setIcon(QVariant ic){
    _icon = ic;
}

QVariant TreeItem::getIcon(){
    return _icon;
}

void TreeItem::setState(QVariant st)
{
    _state = st;
}

QVariant TreeItem::getState()
{
    return _state;
}

bool TreeItem::isChecked(){
    return m_checked;
}
 
void TreeItem::setCheckState(bool state){
    m_checked = state;
}
