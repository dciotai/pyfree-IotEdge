#include "producer_aliyun.h"

#include "Log.h"
#ifdef WIN32
#define usleep(x) Sleep(x)
#endif


ProducerAliyun::ProducerAliyun()
	: running(true)
{

}

ProducerAliyun::~ProducerAliyun()
{
	running = false;
}

void* ProducerAliyun::run()
{
	while (running)
	{
		usleep(100);
	}
	return NULL;
}
