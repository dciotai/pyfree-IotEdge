#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _APP_CONF_DATA_H_
#define _APP_CONF_DATA_H_
/***********************************************************************
  *Copyright 2020-05-04, pyfree
  *
  *File Name       : conf_read.h
  *File Mark       : 
  *Summary         : 
  *程序运行参数集,由单体类业务数据实例继承调用
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include "conf_app.h"

class AppConfData
{
protected:
    /* data */
    //app运行参数配置集
	pyfree::AppConf appConf;
public:
	//mqtt配置
	inline bool getMqttFunc()
	{
		return appConf.mqttconf.mqttFunc;
	};
	inline pyfree::MqttConf getMqttConf()
	{
		return appConf.mqttconf;
	};
	//
	
	inline std::string getGLogDir()
	{
		return appConf.saconf.gLogDir;
	};
	
	inline std::string getTranIP()
	{
		return appConf.lconf.ipclient;
	};
	inline int getTranPort()
	{
		return appConf.lconf.portClient;
	};
	
	inline char getDiskSymbol()
	{
		return appConf.saconf.diskSymbol;
	};
	inline int getFreeSizeLimit()
	{
		return appConf.saconf.freeSizeLimit;
	};
	inline int getDayForLimit()
	{
		return appConf.saconf.dayForLimit;
	};
	//socket for gather
	inline std::string getGatherIp()
	{
		return appConf.lconf.gather_ip;
	};
	inline int getGatherPort()
	{
		return appConf.lconf.gather_port;
	};
	//area info
	inline pyfree::AreaInfo getAreaInfo()
	{
		return appConf.ainfo;
	};
	//record
	inline bool getRecord_Func()
	{
		return appConf.recconf.recordFunc;
	};
	inline std::string getRecord_PeerIp()
	{
		return appConf.recconf.peer_ip;
	};
	inline int getRecord_PeerPort()
	{
		return appConf.recconf.peer_port;
	};
	inline std::string getRecord_LocalIp()
	{
		return appConf.recconf.local_ip;
	};
	inline int getRecord_LocalPort()
	{
		return appConf.recconf.local_port;
	};
	//waring_push
	inline bool getWP_Func()
	{
		return appConf.wpconf.wpFunc;
	};
	inline std::string getWP_PeerIp()
	{
		return appConf.wpconf.peer_ip;
	};
	inline int getWP_PeerPort()
	{
		return appConf.wpconf.peer_port;
	};
	inline std::string getWP_LocalIp()
	{
		return appConf.wpconf.local_ip;
	};
	inline int getWP_LocalPort()
	{
		return appConf.wpconf.local_port;
	};
	//local socket for client
	inline bool getLocalSocketFunc()
	{
		return appConf.lconf.localSocketFunc;
	};
	inline std::string getLocalSocketIp()
	{
		return appConf.lconf.ipLocalSocket;
	};
	inline int getLocalSocketPort()
	{
		return appConf.lconf.portLocalSocket;
	};
	//func
	inline bool getTranFunc()
	{
		return appConf.lconf.tranFunc;
	};
	inline bool getPlanFunc()
	{
		return appConf.bsconf.planFunc;
	};
	inline bool getAlarmFunc()
	{
		return appConf.bsconf.alarmFunc;
	};
	inline bool getTimeUpFunc()
	{
		return appConf.uconf.timeUpFunc;
	};
	inline int getTimeUpInterval()
	{
		return appConf.uconf.timeUpInterval;
	};
	inline int getTimeCheckInterval()
	{
		return appConf.uconf.timeCheckInterval;
	};
	//waring
	inline bool getWaringEventFunc()
	{
		return appConf.uconf.waringEventFunc;
	};
	//verify
	inline bool getVerificationFunc()
	{
		return appConf.uconf.verificationFunc;
	};
	//weather http 
	inline bool getHttpWeatherFunc()
	{
		return appConf.hweather.httpWeatherFunc;
	};
	inline pyfree::HttpWeather getHttpWeather()
	{
		return appConf.hweather;
	};
	//aliyun iot
	inline bool getAliyunIOFunc()
	{
		return appConf.aliconf.aliyunIoFunc;
	};
	inline pyfree::AliyunTriples getGateWay()
	{
		return appConf.aliconf.gateway;
	};
	inline pyfree::ALiyunEventInfo getEventInfo()
	{
		return appConf.aliconf.aliEI;
	};
protected:
    AppConfData(/* args */);
    ~AppConfData();
};

#endif
