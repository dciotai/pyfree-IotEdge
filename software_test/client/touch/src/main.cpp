/****************************************************************************
**
** Copyright (C) 2015 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "qtquickcontrolsapplication.h"

#include <QtQml/QQmlApplicationEngine>
#include <QTextCodec>
#include <QDebug>

#include "clientsocket.h"
//#include "testqml.h"


int main(int argc, char *argv[])
{
//    qputenv("QT_IM_MODULE", QByteArray("qtvirtualkeyboard"));

    QtQuickControlsApplication app(argc, argv);
    //
    QTextCodec *codec = QTextCodec::codecForName("UTF-8");
    QTextCodec::setCodecForLocale(codec);
//    QTextCodec::setCodecForLocale(QTextCodec::codecForLocale());
    //
    QQmlApplicationEngine engine(QUrl("qrc:/main.qml"));
    if (engine.rootObjects().isEmpty())
        return -1;
    QList<QObject*> objl = engine.children();
    for(int i=0; i<objl.size();i++){
        qDebug()<<objl.at(i)->objectName();
    }
    ClientSocket myClass;
//    MyClass myClass;
//    QObject *mview = engine.findChild<QObject*>("appview");
    QObject *mview = engine.rootObjects().first();
    if (mview)
    {
        qDebug() << "connect set!";
        QObject::connect(&myClass, SIGNAL(addDev(QVariant,QVariant,QVariant,QVariant)),
                      mview, SLOT(addDev(QVariant,QVariant,QVariant,QVariant)));
        QObject::connect(&myClass, SIGNAL(addPInfo(QVariant,QVariant,QVariant,QVariant,QVariant,QVariant)),
                      mview, SLOT(addPInfo(QVariant,QVariant,QVariant,QVariant,QVariant,QVariant)));
        QObject::connect(&myClass, SIGNAL(PValue(QVariant,QVariant,QVariant,QVariant)),
                      mview, SLOT(pValue(QVariant,QVariant,QVariant,QVariant)));
        QObject::connect(&myClass, SIGNAL(ipLinkSuccess(QVariant)),
                      mview, SLOT(ipCache(QVariant)));

        QObject::connect(mview, SIGNAL(setPValue(int,int,qreal)),
                      &myClass, SLOT(setPValue(int,int,qreal)));

        QObject::connect(mview, SIGNAL(onIpConf(QString)),
                      &myClass, SLOT(onIpConf(QString)));
    }
    myClass.initIpForView();
    myClass.start();

    return app.exec();
}
