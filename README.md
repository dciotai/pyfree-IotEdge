# pyfree-IotEdge

#### 介绍
c++开发的一套物联网边缘服务系统,提供一整套完整的物联网接入层解决方案，支持跨平台部署，当前主要包含采集和调度两个子系统，采集子系统用于设备的供电、态势、IT资源等信息采集,支持串口、网口(WIFI)、蓝牙、USB等采集接口，以及采集数据的清洗、分析、中转、级联；调度子系统通过可定制的定期、定时、轮询、条件、启停等任务策略以及可视化监控终端实现设备态势的自动化调度与运维，以及部分逻辑计算。
其他通信设备如lora、ZigBee、数传等暂时通过中转设备再以网络、串口等方式接入工控设备。

本着构建一个适应大多数行业物联网边缘端的接入层解决方案，项目不仅涉及到开发部分，还会涉及到工程化、产品化、技术文案等多方面的集成。项目也会收集相关的行业需求，根据项目推进计划集成到项目中，如果使用本项目过程中有任何问题或需求提议，请根据文末的"问题反馈"章节进行反馈，谢谢！  

#### 软件架构
> 软件架构说明  
当前主要采集和调度两个子系统以及配套测试、产品化、工程化的一些辅助模块，如下图所示。   
采集软件主要实现数据的采集、汇聚、归一化处理，并可以进行相互进行级联，实现更大范围的数据汇聚。  
调度软件主要实现数据的清洗、分析、上云以及实现就地的业务调度、逻辑计算。  
记录软件主要实现数据的记录,暂时支持sqlite文本数据库写入。    
告警推送软件主要实现告警信息通过邮件、短信、第三平台等实现告警发布。  
![系统架构图](/res/for_doc/system_com.PNG)  
其中采集软件请查看gather-master目录下的文档说明，调度软件请查看monitor-master目录下文档说明,记录软件请查看record-master目录下的文档说明，告警推送软件请查看waringpush-master目录下的文档说明。    

### 场景示例

* 方案示例
  1. 将软件集中部署在一台工控机，一般工控机具备RS-232/485/422、RJ45、蓝牙、USB等接口，利用这些接口与设备构建通信，完成数据采集。  
  ![简单方案示例](/res/for_doc/business_simple.PNG)   
  2. 同样将软件集中部署在一台工控机上，利用通信设备进行数据采集，例如串口服务器、Lora中继器、光端机、电源控制器、远程IO模块等。  
  ![中等方案示例](/res/for_doc/business_middle.PNG)   
  3. 将软件集中部署在不同的工控机上，利用组网技术构建复杂系统，实现数据采集级联与跨场景联动，例如利用环网交换机的通信设备组网技术，构建通信层，如果距离受限时，可以采用中继器设备进行扩展。  
  ![复杂方案示例](/res/for_doc/business_complex.PNG)   

* 使用场景
  1. 农业大棚  
    （1）一级设备，如温湿度、水质等传感器，水泵、投食机、风机、空调等大型设备，电子水阀、网络摄像头等电子产品  
    （2）二级设备，如IO模块、串口服务器、电源控制器、工控机、路由等  
    （3）一个涵盖水产养殖区、畜牧养殖区、家禽养殖区的农业调度方案，根据传感器采集数据、气象、时间等条件进行农业管理调度，实现供水、清淤、投食等的自动化与联动控制。  
     ![农业方案示例](/res/for_doc/business_agriculture.PNG)  
  2. 多媒体投影的会议室、橱窗、展厅、影院等   
    (1)一级设备，如投影仪、播放器、灯光、 音响、摄像头等设备  
    (2)二级设备，如工控机、4G(WIFI)路由、小型串口服务器、电源控制器等设备   
    (3)一个单套投影的会议室调度系统，根据时间或终端操作实现投影联动，例如到点自动关闭投影条件、关闭相关电源，实现自动化维护。  
    ![多媒体方案示例](/res/for_doc/business_media.PNG)  
  3. 待续...     

#### 编译

1. 当前系统依赖alc-master,libctb,lua,muparser,sqlite,mosquitto,iot_aliyun等第三方库，本目录提供了部分平台已编译过的库，更多平台需要请参考源码编译，具体编译要求请查看采集软件和调度软件的说明文档    
2. 采集软件的编译见gather-master目录下说明[文档](gather-master/README.md)    
3. 调度软件的编译见monitor-master目录下说明[文档](monitor-master/README.md)    
4. 记录软件的编译见record-master目录下说明[文档](record-master/README.md)  
5. 服务管理软件的编译见sysmgr-master目录下说明[文档](sysmgr-master/README.md)  
6. 辅助测试软件见software_test目录下的说明文档  

#### 目录结构

* acl-master  
acl_master是一个跨平台c/c++库，提供了网络通信库及服务器编程框架，同时提供更多的实用功能库及示例，具体编译与实现请参考其说明文档。  
项目地址: https://gitee.com/acl-dev/acl 或 https://github.com/acl-dev/acl  
技术博客：https://www.iteye.com/blog/user/zsxxsz  

* common  
一些项目用到的公共代码,一些通用的函数集或类  

* demo-project  
项目样例,涵盖项目一整套解决方案的demo,各个样例会有关联，详见各个说明文档、配置文档      

* gather-master  
采集软件工程目录  

* iot_aliyun  
已编译好的阿里云物联网平台的接口库，基于iotkit-embedded-2.3.0.zip源码编译  

* libctb  
libctb是一个c++实现串口(serial/uart/gpib)通信的程序库，下载:https://www.iftools.com/download/index.en.php，搜索libctb可以下载，
或者使用本项目提供的third_org\libctb.zip

* lua  
已经编译好的c++调用lua脚本的接口库，其源码下载地址：http://www.lua.org/ftp/ 或者使用本项目提供的third_org\lua.zip
也可以直接采用本项目编译好的库（Lua 5.3.0）。  

* monitor-master  
调度软件工程目录  

* mosquitto  
已经编译好的mqtt通信的程序库,其下载地址：http://mosquitto.org/download/ 在third_org目录下提供了源码包mosquitto-1.5.7.tar.gz

* muparser  
muparser是一个计算公式库，快速和简单地解析数学表达式及进行计算，本项目主要用来处理计算点数值计算，
其源码本项目经过整理为cmake工程，直接进入到muparser目录按cmake编译即可  

* record-master  
记录软件工程目录,从调度软件获得信息点态势选择存储方式实时记录。      

* res  
项目设计、开发相关的素材资源  

* software_test   
辅助项目搭建测试环境的相关程序源码，目前主要有播放器、本地监控终端程序  

* sqlite   
sqlite为sqlite数据库的c接口，源码本项目经过整理为cmake工程，直接进入到sqlite目录按cmake编译即可 

* svc_common  
程序转为后台运行相关的通用源码  

* swLicense  
项目辅助程序,根据磁盘或网卡信息生成license的程序工程及源码  

* sysmgr-master  
服务管理程序,对采集服务、调度服务进行态势监控以及其配置文件上传、下载管理  

* third_org  
第三方库源码

* uuid  
已编译号的libuuid,主要用于生成uuid使用，linux下需要，其下载地址：https://sourceforge.net/projects/libuuid/   
或者使用本项目提供的third_org/libuuid-1.0.3.tar.gz  

* waringpush-master  
告警推送程序,从调度软件获得告警信息,按需采用多种方式通知责任人或发布到第三平台。    

#### 更新说明
v1.01版本  
1.  采集软件linux版本:串口、网口采集，网口、串口(待测试)转发，网口级联  
2.  调度软件linux版本:数据记录、分析、告警、调度，采集软件接口、客户端接口、阿里云物联网平台接口、mqtt接口  
3.  辅助测试软件:支持网络监控的播放器、本地可视化客户端  

v1.02版本  
1.  win支持,当前版本支持cmake+vs2015编译,具体编译说明,看采集软件和调度软件的说明文档   
2.  win服务支持,支持win系统下采集软件、调度软件作为服务的安装、启停、运行、卸载添加   

v1.03版本  
1.  采集软件、调度软件的linux版本docker部署测试,基于centos:7镜像测试  
2.  采集软件、调度软件的win/linux版本服务安装部署调整,为系统管理辅助软件做准备  

v1.04版本   
1.  服务管理软件完成开发以及linux测试(阿里云物联网平台OTA固件升级待测)   
2.  提供远程管理采集软件、调度软件等服务程序的能力,暂时支持本地web可视化管理和阿里云物联网平台管理   
3.  服务管理软件完成win平台编译测试(阿里云物联网平台OTA固件升级待测)   
4.  项目的linux版本支持将git信息写入程序版本描述信息和日志输出   
5.  源代码统一为tab制表符(4空格),UTF-8编码,CRLF行结尾格式    
6.  服务管理软件阿里云物联网接口win联调测试(采集、调度服务)  

v1.05版本  
1.  代码优化、联调测试、压力测试，为发行版做准备   
2.  gather软件在gathermgr类中归一化采集端口、转发端口的,下级采集网络级联socket_gather类继承gchannel类,删除socket_gather_data.h/cpp,其实现代码并入socket_gather.h/cpp中。  
3. 删除servicechain.h/cpp源代码,其创建任务编号的功能移到pfunc_time.h/cpp中实现。  
4. 监控软件解决Business_def类臃肿问题,将实施横面数据处理业务构建新类HorizontalData，将程序运行参数业务数据处理业务构建新类AppConfData,Business_def继承两个新建类，并调整了一些业务函数实现。    
5.  打印输出调整     
6.  历史数据查看工具  

v1.06版本  
1.  调整采集软件的转发接口,将gather-master/channel-conf/tchannel.h(cpp)将非共性的功能搁置到子类实现。  
2.  增加串口转发与级联,源码详见gather-master/serial-gather/serialtchannel.h(cpp) ,而脚本解析样例详见demo-project/bgather/modbus_tran.lua。当前版本中串口转发模拟从站端,仅是实现总召查询、单点查询、单点控制的功能，遵循从站端的接收上层请求-业务处置-及时响应的应答通信方式。  
3.  调整串口转发通信模式,在gather.xml的serialport配置中加入了ComModel用来标识通信模式[1,应答 默认; 2,双向发送,全态势推送;2,双向发送, 单点态势推送],类型2/3主要通过串口建立通信信道，采用类似tcp双向通信(上层下控直接写入，巡检接口读数,下层变位或定期写入,巡检接口读数),可以通过demo-project/tool/sscom.exe以及mbslave.exe辅助工具快速体验(包括应答方式),配置参考demo-project/bgather目录下的gather.xml和modbus_tran.lua。          

v1.07版本  
1.  将记录功能从调度软件独立出来,建立记录软件(服务),为调度软件裁减和其他平台移植做准备    
2.  调度软件与记录软件采用udp通信,调度软件发送->记录软件接收  
3.  为采集、调度、记录、管理软件添加旧日志删除功能,记录软件添加删除旧历史数据功能,均来自common/spaceMgr.h/cpp源码实现   
4.  添加记录软件的样例到demo-project目录  

v1.08版本  
1.  将告警信息发布功能从调度软件独立出来,建立告警推送软件(服务),为调度软件裁减和其他平台移植做准备
2.  调度软件与告警推送软件采用udp通信,调度软件发送->记录软件接收,基于告警内容扩展,采用通信内容格式为json格式。  
3.  添加告警推送软件的样例到demo-project/waringpush目录   
4.  cmake+vs201*编译程序出现许多未定义原因(见附录01) 
6.  采集软件的串口转发接口的请求-应答模式( ComModel="1")增加多点查询功能,多点查询时,返回的val不再为点值,而是查询点数,具体样例可以查看demo-project/bgather的gather.xml和modbus_tran.lua配置文件,可以借助demo-project/tool/modbus-Slave.exe和sscom5.exe工具测试体验  


#### 近期计划

1.  日志输出重构及日志分析  
2.  蓝牙、USB等采集接口支持  
3.  apr刺探，IT资源采集(本机cpu/内存/磁盘/关键进程)  
4.  远程可视化配置  
5.  坐标定位、实时节日等云服务接口支持  

#### 问题反馈

1)可以在本项目主题下留言    
2)py8105@163.com   
3)一般会在晚上或周末进行回复  

####  附录
01.  下载的程序在cmake构造vs201*的工程在编译时许多未定义错误出现，在于源码本身是统一设定为Windows(CRLF)-UTF格式的头文件,被调整了Unix(LF)-UTF格式,造成无法加载头文件而出现未定义错误,为何从源码库下载文件后，会出现文件格式被调整原因暂时不明，当前暂时采用文本编译工具调整会Windows(CRLF)-UTF格式解决。  
