#include "pfunc_qt.h"

#include <QDebug>

int PFUNC_QT::getChlId(QString _chlname)
{
    int _strfstart = _chlname.lastIndexOf("(");
    int _strfend = _chlname.lastIndexOf(")");
    if (_strfstart>0&&_strfend>_strfstart)
    {
        QString _chlindex = _chlname.midRef(_strfstart+1,_strfend-_strfstart-1).toString();
        if (_chlindex.toInt()>0)
        {
            return _chlindex.toInt();
        }
    }

    return -1;
};

unsigned int PFUNC_QT::getChlUId(QString _chlname)
{
    int _strfstart = _chlname.lastIndexOf("(");
    int _strfend = _chlname.lastIndexOf(")");
    if (_strfstart>0&&_strfend>_strfstart)
    {
        QString _chlindex = _chlname.midRef(_strfstart+1,_strfend-_strfstart-1).toString();
        return _chlindex.toUInt();
    }

    return 0;
};

unsigned int PFUNC_QT::getPUId(QString _pname)
{
    int _strfstart = _pname.lastIndexOf("[");
    int _strfend = _pname.lastIndexOf("]");
    if (_strfstart>0&&_strfend>_strfstart)
    {
        QString _pindex = _pname.midRef(_strfstart+1,_strfend-_strfstart-1).toString();
        return _pindex.toUInt();
    }

    return 0;
}

QString PFUNC_QT::getDateTimeFormat()
{
    return QString("yyyy-MM-dd hh:mm:ss.zzz");
}

QString PFUNC_QT::getDateFormat()
{
    return QString("yyyy-MM-dd");
}

QString PFUNC_QT::getTimeFormat()
{
    return QString("hh:mm:ss.zzz");
}

QString PFUNC_QT::ChangeDateTime(const QDateTime m_QDateTime)
{
    return m_QDateTime.toString("yyyy-MM-dd hh:mm:ss.zzz");
};

//
QDateTime PFUNC_QT::ChangeStr(const QString m_DateTimeStr)
{
    return QDateTime::fromString(m_DateTimeStr,"yyyy-MM-dd hh:mm:ss.zzz");
};

QString PFUNC_QT::ChangeDateTimeS(const QDateTime m_QDateTime)
{
    return m_QDateTime.toString("yyyy-MM-dd hh:mm:ss");
};

//
QDateTime PFUNC_QT::ChangeStrS(const QString m_DateTimeStr)
{
    return QDateTime::fromString(m_DateTimeStr,"yyyy-MM-dd hh:mm:ss");
};

QDate PFUNC_QT::ChangeDateStr(const QString m_DateStr)
{
    return QDate::fromString(m_DateStr,"yyyy-MM-dd");
};

QString PFUNC_QT::ChangeDate(const QDate m_QDate)
{
    return m_QDate.toString("yyyy-MM-dd");
};

QTime PFUNC_QT::ChangeTimeStr(const QString m_QTimeStr)
{
    return QTime::fromString(m_QTimeStr,"hh:mm:ss.zzz");
};

QString PFUNC_QT::ChangeTime(const QTime m_QTime)
{
    return m_QTime.toString("hh:mm:ss.zzz");
};

QDateTime PFUNC_QT::ChangeLong(const long long m_DateLong)
{
    return QDateTime::fromMSecsSinceEpoch(m_DateLong);
}

long long PFUNC_QT::ChangeDateTimeL(const QDateTime m_QDateTime)
{
    return m_QDateTime.toMSecsSinceEpoch();
}

QDateTime PFUNC_QT::ChangeLongLong(const long long m_DateLong)
{
    return QDateTime::fromMSecsSinceEpoch((m_DateLong - 116444736000000000L)/10000);
};

long long PFUNC_QT::ChangeDateTimeLL(const QDateTime m_QDateTime)
{
    return (m_QDateTime.toMSecsSinceEpoch()*10000+116444736000000000L);
}