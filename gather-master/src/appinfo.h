////版本源文件
/***********************************************************************
  *Copyright 2020-04-21, pyfree
  *
  *File Name       : app info.h
  *File Mark       : 
  *Summary         : 软件版本信息描述
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#pragma once

#define VERSION_BUILD_NUMBER 	145cb21
#define STR_BUILD_NUMBER 		"145cb21"
#define STR_VERSION_BUILD 		"(B145cb21 2020-07-11 14:19:41)"
#define STR_VERSION_DATE 		"2020-06-27 16:18:29"
#define STR_VERSION_URL 		"https://"
#define STR_VERSION_NOW 		"2020-07-11 14:19:41"
#define NSTR_VERSION_NOW 		2020-07-11 14:19:41

#define STR_VERSION_VERSION		"v1.08"
#define STR_VERSION_COMPANY		"科技有限公司"
#define STR_VERSION_FILE	    "pyfree-gather"
#define STR_VERSION_FILEDESC	"采集软件"
#define STR_VERSION_FILEVER		STR_VERSION_VERSION
#define STR_VERSION_LEGAL		  "Copyright(C) 2020 All rights reserved"
#define STR_VERSION_PRODUCTNAME	"采控软件"
#define STR_VERSION_TELE		  "400-602-39188"
#define STR_VERSION_FAX			  "400-602-39188"	
#define STR_VERSION_WEB			  "https://gitee.com/pyzxjfree/pyfree-IotEdge"
#define STR_VERSION_POSTCODE	"519080"
#define STR_VERSION_ADDRESS		"珠海"
#define STR_VERSION_DPEMAIL   	"py8105@163.com"
