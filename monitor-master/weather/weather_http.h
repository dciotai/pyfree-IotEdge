#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _WEATHER_HTTP_H_
#define _WEATHER_HTTP_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : weather_http.h
  *File Mark       :
  *Summary         : 
  *该线程向调用新闻api接口获取新闻资讯、天气预报等信息,http访问采用acl库实现
  *Current Version : 1.00
  *Author          : PengYong
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include "lib_acl.h"
#include "acl_cpp/lib_acl.hpp"

#include "Mutex.h"
#include "datadef.h"

namespace acl
{
	class http_request;
};
//聚合天气信息结构化表述
struct WeatherData
{
	WeatherData()
		: city("")
		, date("")
		, time_("")
		, week("")
		, temperature("")
		, temperature_l("")
		, temperature_h("")
		, weather("")
		, weather_ID_FA("")
		, weather_ID_FB("")
		, wind("")
		, uv_index("")
		, curDate("")
		, curTime("")
		, sh("")
		, eh("")
	{};
	WeatherData& operator = (const WeatherData& rhs) //赋值符重载  
	{
		if (this==&rhs)
		{
			return *this;
		}
		city = rhs.city;
		date = rhs.date;
		time_ = rhs.time_;
		week = rhs.week;
		temperature = rhs.temperature;
		temperature_h = rhs.temperature_l;
		temperature_h = rhs.temperature_h;
		weather = rhs.weather;
		weather_ID_FA = rhs.weather_ID_FA;
		weather_ID_FB = rhs.weather_ID_FB;
		wind = rhs.wind;
		uv_index = rhs.uv_index;
		curDate = rhs.curDate;
		curTime = rhs.curTime;
		sh		= rhs.sh;
		eh		= rhs.eh;
		return *this;
	}

	std::string city;
	std::string date;
	std::string time_;
	std::string week;
	std::string temperature;
	std::string temperature_l;
	std::string temperature_h;
	std::string weather;
	std::string weather_ID_FA;
	std::string weather_ID_FB;
	std::string wind;
	std::string uv_index;
	std::string curDate;
	std::string curTime;
	std::string sh;
	std::string eh;

};

struct HttpApiArgWeather
{
	HttpApiArgWeather()
		: startTime(8)
		, endTime(8)
		, addr_("weather-ali.juheapi.com")
		, path_("/weather/index")
		, port_(80)
		, dtype_("json")
		, format_(1)
		, city_("珠海")
		, authorization("APPCODE e64e5d45869e4df588326420859da9dc")
		, upTime(60)
		, isThreeHourCity(false)
	{

	};
	int startTime;				//开始抓取信息时间（单位小时）
	int endTime;				//结束抓取信息时间（单位小时）
	std::string addr_;			// web 服务器地址  
	std::string path_;			// 本地请求的数据文件  
	int port_;					//
	std::string dtype_;			//返回数据格式：json或xml,默认json
	int format_;				//未来6天预报(future)两种返回格式，1或2，默认1
	std::string city_;			//
	std::string authorization;	// 访问授权
	int upTime;					//天气预报更新间隔，单位秒
	bool isThreeHourCity;		//是否为3小时的城市天气预报
};

class WeatherHttp : public acl::thread
{
public:
	WeatherHttp();
	~WeatherHttp(void);

	void* run();

	bool getCurWeatherDesc(std::string &str);
	bool getCurWeatherDesc(WeatherData &wd);
private:
	bool mapCurTime();
	void getHttpInfo();

	//天气信息返回解析
	bool do_plain(acl::http_request& req);
	//bool do_xml(acl::http_request& req);
	void getCurTVHour(int &hour_,std::string &date_);
	bool do_json(acl::http_request& req);

	void add(WeatherData wd);
private:
	bool running;				//线程运行标记
	//
	HttpApiArgWeather myArgHttp;
	unsigned int timeForData;

	WeatherData weatherData;	//天气缓存信息
	PYMutex m_MutexWeatherData;	//更新天气的线程锁
};

#endif
