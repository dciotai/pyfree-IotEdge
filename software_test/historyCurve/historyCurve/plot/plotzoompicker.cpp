#include "plotzoompicker.h"

MyQwtPlotZoomer::MyQwtPlotZoomer( QwtPlotCanvas *_canvas, bool doReplot )
    : QwtPlotZoomer(_canvas,doReplot)
{
};
MyQwtPlotZoomer::MyQwtPlotZoomer( int xAxis, int yAxis,QwtPlotCanvas *_canvas, bool doReplot )
    : QwtPlotZoomer(xAxis,yAxis,_canvas,doReplot)
{
};

void MyQwtPlotZoomer::widgetMousePressEvent( QMouseEvent *event )
{
    // if (Qt::MidButton==event->button())
    // {
    //     emit resetcanvas();
    // }
    if (Qt::MidButton==event->button())
        emit resetcanvas();
    else
        QwtPlotZoomer::widgetMousePressEvent(event);
}

void MyQwtPlotZoomer::widgetMouseReleaseEvent( QMouseEvent *event )
{
    // if (Qt::MidButton==event->button())
    // {
    //     emit resetcanvas();
    // }else{
    //     QwtPlotZoomer::widgetMouseReleaseEvent(event);
    // }
    if (Qt::MidButton!=event->button())
        QwtPlotZoomer::widgetMouseReleaseEvent(event);
}

void MyQwtPlotZoomer::widgetMouseDoubleClickEvent( QMouseEvent *event )
{
    if (Qt::LeftButton==event->button())
    {
        emit resetcanvas();
        // if (zoomStack().size()>1)
        // {
        //     this->zoom(0);
        //     this->rescale();
        // }
    }

    QwtPlotZoomer::widgetMouseDoubleClickEvent(event);
};

void MyQwtPlotZoomer::clearZoomStack()
{
    this->setZoomBase();
};