#ifndef ROUTING_H
#define ROUTING_H
#include <QThread>

QT_BEGIN_NAMESPACE
class Player;
QT_END_NAMESPACE

class Routing : public QThread
{
    Q_OBJECT
public:
    Routing(Player *_player
            ,QObject * parent = 0);
    ~Routing();
protected:
    void run();
signals:
    void sendItem(int id,int val);
public slots:
    void Tolcall();
private:
    Player *player;
};

#endif // ROUTING_H
