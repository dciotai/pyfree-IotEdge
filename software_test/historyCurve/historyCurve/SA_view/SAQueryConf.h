#ifndef AGCQUERYCONF_H
#define AGCQUERYCONF_H

#include <QWidget>

class SAFileTableView;
class QDateTimeEdit;
class QComboBox;

class SAQueryConfView : public QWidget
{
	Q_OBJECT
public:
	SAQueryConfView(QWidget * parent = 0, Qt::WindowFlags f = 0);
	~SAQueryConfView();

	void setTimeRegion(long long _startT,long long _endT);
	void getTimeRegion(long long &_startT,long long &_endT);
	void setDevIDList(QSet<int> _devids);
	void setIDXList(QSet<int> _idxs);
signals:
	void fileSelect(QString _name);
	void ReloadSA();
	void draw(int _devid,int _idx);
	void resetCurveType(int _curveType);
	void idxCheck(int _idx);
private slots:
	void DrawCurve();
private:
	SAFileTableView *ptr_SAFileTableView;
	QDateTimeEdit *myStartT;
	QDateTimeEdit *myEndT;
	QComboBox *dev_combo;
	QComboBox *idx_combo;
	QComboBox *curve_Type;
};

#endif //AGCQUERYCONF_H
