#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _BUSINESS_DEF_H_
#define _BUSINESS_DEF_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : conf_read.h
  *File Mark       : 
  *Summary         : 
  *业务数据集的前置声明,属于单体类,相当于业务数据相关的全局变量+全局函数集
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include "conf_read.h"
#include "hashmap.h"
#include "datadef.h"
#include "dtypedef.h"
#include "conf_app.h"
#include "conf_dev.h"
#include "conf_pmap.h"
#include "conf_plan.h"
#include "conf_alarm.h"
#include "conf_war.h"
#include "queuedata_single.h"
#include "horizontal_data.h"
#include "appconf_data.h"

class RecordQueue;

// bool comp_dev(const pyfree::Dev &a, const pyfree::Dev &b);//仅在socket_local_write.cpp,已经迁移至该源文件

class BusinessDef : public HorizontalData , public AppConfData
{
public:
	static BusinessDef* getInstance();
	static void Destroy();
	~BusinessDef();
public:
	//////////////////////////////////for get func///////////////////////////////////////////////
	inline std::vector<pyfree::Plan> getPlans()
	{
		return plans;
	};
	inline std::vector<pyfree::Alarm> getAlarms()
	{
		return alarms;
	};
public:
	bool setValue(std::string _ipStr, int _id, pyfree::PType _pType, float _val, unsigned long _taskID = 0);
	bool getFromInfo(unsigned long long _devID, unsigned int _pID, pyfree::PFrom &_pfrom);
	bool getToInfo(std::string _ipStr, int _id, pyfree::PType _pType,pyfree::PTo &_pto);

	void TimeUpVirtualPInfo();	//定期推送虚拟点信息
	void TimeCheckPInfo();		//定期校对信息点数据
protected:
	void send_data_to_third(const DataToThird &_wdlc,bool ali_flag=false);
	void add_event(EventForWaring event_);
private:
	BusinessDef();
	BusinessDef& operator=(const BusinessDef&) { return *this; };
	void init(); 
private:
	static BusinessDef* instance;
	// RecordQueue *ptr_rq;
	//
	QueueDataSingle<RecordItem> *queuefor_record;	//记录缓存队列
	//
	QueueDataSingle<EventForWaring> *queueforwar;//告警事件缓存队列
	//
	QueueDataSingle<SocketLocalWriteItem> *queueforwrite_local;
	//
	QueueDataSingle<SocketAliyunWriteItem> *queueforwrite_aliyun;
	//
	QueueDataSingle<SocketMqttWriteItem> *queueforwrite_mqtt;
	
	//
	//信息点映射配置
	std::vector<pyfree::PMap> pmaps;
	//任务策略
	std::vector<pyfree::Plan> plans;
	//告警策略
	std::vector<pyfree::Alarm> alarms;
	//映射转换,提高效率
	MyObj_FT<pyfree::PTo> ftmap;
	MyObj_TF<pyfree::PFrom> tfmap;
};

#endif


