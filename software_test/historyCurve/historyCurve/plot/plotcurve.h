#ifndef PLOTCURVE_H
#define PLOTCURVE_H

#include "qwt/qwt_plot_curve.h"
#include "qwt/qwt_scale_map.h"
#include "qwt/qwt_symbol.h"
#include "plotData.h"

class MyQwtPlotCurve : public QwtPlotCurve
{
public:
    explicit MyQwtPlotCurve(const QString &title = QString::null);
    explicit MyQwtPlotCurve(const QwtText &title);
    ~MyQwtPlotCurve();

    virtual void drawSeries( QPainter *,
        const QwtScaleMap &xMap, const QwtScaleMap &yMap,
        const QRectF &canvasRect, int from, int to ) const;

    void setCurveInterval( QVector<CurveInterval> &rValue);
    void setCurveMove( const double x_Move, const double y_Move);
protected:
    virtual void drawCurve( QPainter *p, int style,
        const QwtScaleMap &xMap, const QwtScaleMap &yMap,
        const QRectF &canvasRect, int from, int to ) const;
    virtual void drawSymbols( QPainter *p, const QwtSymbol &,
        const QwtScaleMap &xMap, const QwtScaleMap &yMap,
        const QRectF &canvasRect, int from, int to ) const;
    void drawMyLines( QPainter *p,
        const QwtScaleMap &xMap, const QwtScaleMap &yMap,
        const QRectF &canvasRect, int from, int to ) const;
protected:
    void dataChanged();
	size_t dataSize() const;
    QRectF dataRect() const;
	void setRectOfInterest( const QRectF &rect );
private:
    void MyInit();
private:
    QVector<CurveInterval> *m_CurveInterval;
    double XMove;
    double YMove;
};

#endif //PLOTCURVE_H
