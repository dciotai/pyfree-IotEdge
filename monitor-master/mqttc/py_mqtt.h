#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _PY_MQTT_CPP_H_
#define _PY_MQTT_CPP_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : py_mqtt.h
  *File Mark       :
  *Summary         : 
  *mqtt业务接口,基于mosquitto的c++库实现,需要重写mqtt通信以及数据的发布订购等回调函数
  *
  *Current Version : 1.00
  *Author          : PengYong
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include<iostream>

#include <mosquittopp.h>

#include "queuedata.h"

class PyMQTTIO : public mosqpp::mosquittopp
{
public:
	PyMQTTIO(const char *id);//note the id for every client is cann't equal

	/*
	*on_connect on_disconnect on_publish on_subscribe on_message这几个函数是mqtt接口回调函数,
	*是mosquittopp父类的虚函数的具体实现
	*/
	void on_connect(int rc);
	void on_disconnect();
	void on_publish(int mid);
	void on_subscribe(int mid, int qos_count, const int *granted_qos);	//订阅回调函数
	void on_message(const struct mosquitto_message *message);			      //订阅主题接收到消息

	/**
	 * 从mqtt服务端下发消息的缓存队列中获取队列头元素信息
	 * @param it {string& } 输出信息项
	 * @return {bool} 是否成功
	 */
	bool pop_rec(std::string &it);

	/**
	 * 向推送消息给mqtt服务端的缓存队列末添加消息
	 * @param it {string } 被添加信息项
	 * @return {bool} 是否成功
	 */
	void add_rep(std::string it);
	/**
	 * 从向mqtt服务端推送消息的缓存队列中获取队列头元素信息
	 * @param it {string& } 输出信息项
	 * @return {bool} 是否成功
	 */
	bool pop_rep(std::string &it);
public:
	bool link;                          //mqtt连接标记,将由消费者和生产者接口维护
	std::string sub_s;                  //订购主题,由消费者接口重设
	QueueData<std::string> *rec_queue;  //来自mqtt服务端的缓存数据,由回调函数接收到数据后写入,由消费者接口读取解析
	QueueData<std::string> *rep_queue;  //由消费者接口写入,由生产者接口读取向mqtt服务器推送
};

#endif


