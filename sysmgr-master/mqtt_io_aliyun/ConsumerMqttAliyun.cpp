#include "ConsumerMqttAliyun.h"

#ifdef WIN32
#include <Windows.h>
#define usleep(x) Sleep(x)
#endif

#include "cJSON.h"
#include "Log.h"
#include "svc_control.h"

ConsumerMqttAliyun::ConsumerMqttAliyun(std::map<std::string, AliyunServiceDesc> subTopicMaps_)
	: running(true)
	, subTopicMaps(subTopicMaps_)
	, rec_queue(QueueDataSingle<RCacheAliyun>::getInstance())
{
	//
}

ConsumerMqttAliyun::~ConsumerMqttAliyun()
{
	running = false;
}


void* ConsumerMqttAliyun::run()
{
	while (running)
	{
		receive();
		usleep(100);
	}
	return 0;
}
/*
std::string Json_decode(const std::string payload)
{
	std::string cp_payload = payload;
	std::string new_payload = "";
	bool lflag = false;
	for(int i=0; i<cp_payload.size();i++)
	{
		new_payload += cp_payload.at(i);
		if (lflag 
			&& ('0' <= cp_payload.at(i) && '9' >= cp_payload.at(i))
			&& (i + 1)<cp_payload.size()
			&& ('0' > cp_payload.at(i + 1) || '9' < cp_payload.at(i + 1))) {
			new_payload += '\"';
			lflag = false;
		}
		if (':' == cp_payload.at(i)&&
			((i+1)<cp_payload.size()
			&& ('-' == cp_payload.at(i+1)||( '0' <= cp_payload.at(i + 1) && '9' >= cp_payload.at(i + 1)))))
		{
			new_payload += '\"';
			lflag = true;
		}
		
	}
	return new_payload;
};
*/
void ConsumerMqttAliyun::receive()
{
	RCacheAliyun item_cache;
	if (rec_queue->pop(item_cache))
	{
		// printf("topic:%s\npayload:%s\n", item_cache.topic.c_str(), item_cache.payload.c_str());
		std::map<std::string, AliyunServiceDesc>::iterator it = subTopicMaps.find(item_cache.topic);
		if (it != subTopicMaps.end())
		{
			try{
			cJSON *request_root = NULL;
			request_root = cJSON_Parse(item_cache.payload.c_str());
			if (request_root == NULL) {
				CLogger::createInstance()->Log(MsgInfo,"JSON Parse Error");
				return;
			}
			if (!cJSON_IsObject(request_root)) {
				CLogger::createInstance()->Log(MsgInfo,"JSON isn't Object Error");
				cJSON_Delete(request_root);
				return;
			}
			cJSON *item_propertyid = NULL, *item_params = NULL;
			item_propertyid = cJSON_GetObjectItem(request_root, "params");
			if(NULL==item_propertyid)
			{
				// CLogger::createInstance()->Log(MsgInfo,
				// 	"cJSON_GetObjectItem for item_propertyid Error,topic:%s\npayload:%s"
				// 	, item_cache.topic.c_str(), item_cache.payload.c_str());
				return;
			}
			if(!cJSON_IsObject(item_propertyid))
			{
				CLogger::createInstance()->Log(MsgInfo,
					"item_propertyid isn't Object Error");
				return;
			}
			for (int j = 0; j < cJSON_GetArraySize(item_propertyid); j++)
			{
				item_params = cJSON_GetArrayItem(item_propertyid, j);
				if(NULL==item_params)
				{
					CLogger::createInstance()->Log(MsgInfo,
						"item_params is null!");
						continue;
				}
				if(NULL==item_params->string)
				{
					CLogger::createInstance()->Log(MsgInfo,
						"item_params string is null!");
					continue;
				}
				if (cJSON_IsNumber(item_params))
				{
					Print_NOTICE("Property ID, index: %d, text: %s, Value: %f\n"
						, j, item_params->string, item_params->valuedouble);
					std::map<std::string, pyfree::ServiceInfo>::iterator itp = it->second.triples.find(item_params->string);
					if (itp != it->second.triples.end())
					{
						pyfree::svc_control(itp->second.svc_name, static_cast<int>(item_params->valuedouble));
					}
				}
				if (cJSON_IsBool(item_params))
				{
					Print_NOTICE("Property ID, index: %d, text:%s, Value: %d\n"
						, j, item_params->string, (item_params->valuedouble > 0 ? 1 : 0));
				}
				if (cJSON_IsString(item_params))
				{
					Print_NOTICE("Property ID, index: %d, text:%s, Value: %s\n"
						, j, item_params->string, item_params->valuestring);
				}
			}
			cJSON_Delete(request_root);
			}
			catch (...) {
				CLogger::createInstance()->Log(MsgInfo
					, "ConsumerMqttAliyun::receive() error!");
			}
		}
	}
};

