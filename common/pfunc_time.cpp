#include "pfunc_time.h"

#include <time.h>
#include <sys/timeb.h>
#ifdef __linux__
#include<sys/time.h>
#endif

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "pfunc_print.h"

std::string pyfree::getCurrentTime()
{
	time_t _t = time(NULL);
	struct tm _tt;
#ifdef WIN32
	localtime_s(&_tt, &_t);
#else
	localtime_r(&_t, &_tt);
#endif
	_tt.tm_year += 1900;
	_tt.tm_mon += 1;
	char buf[32] = { 0 };
	sprintf(buf, "%04d-%02d-%02d %02d:%02d:%02d"
		, _tt.tm_year, _tt.tm_mon, _tt.tm_mday, _tt.tm_hour, _tt.tm_min, _tt.tm_sec);
	return std::string(buf);
}

unsigned long  pyfree::getTaskIDFromDateTime(int index)
{
	struct tm _tt;
	struct timeb tt_;
	ftime(&tt_);
	time_t _t = tt_.time;
#ifdef WIN32
	localtime_s(&_tt, &_t);//系统本地时间
#else
	localtime_r(&_t, &_tt);
#endif
	_tt.tm_year = 0;
	unsigned long tid = static_cast<unsigned long>(100000000 * _tt.tm_hour
		+ 1000000 * _tt.tm_min + 10000 * _tt.tm_sec + 10 * tt_.millitm + index);
	return (tid&0XFFFFL);
}

std::string pyfree::getCurrentTimeByFormat(std::string format, bool localf)
{
	time_t _t = time(NULL);
	struct tm _tt;
#ifdef WIN32
	if (localf)
	{
		localtime_s(&_tt, &_t);//系统本地时间
	}
	else
	{
		gmtime_s(&_tt, &_t);//格林时间
	}
#else
	if (localf)
	{
		localtime_r(&_t, &_tt);//
	}
	else
	{
		gmtime_r(&_t, &_tt);//格林时间
	}
#endif
	_tt.tm_year += 1900;
	_tt.tm_mon += 1;
	char buf[64] = { 0 };
	sprintf(buf, format.c_str()
		, _tt.tm_year, _tt.tm_mon, _tt.tm_mday, _tt.tm_hour, _tt.tm_min, _tt.tm_sec);
	return std::string(buf);
};

unsigned int  pyfree::getCurDayTime(bool localf)
{
	time_t _t = time(NULL);
	struct tm _tt;
#ifdef WIN32
	if (localf)
	{
		localtime_s(&_tt, &_t);//系统本地时间
	}
	else
	{
		gmtime_s(&_tt, &_t);//格林时间
	}
#else
	//Linux 函数localtime()是个static,返回的内存需不需要delete
	if (localf)
	{
		localtime_r(&_t, &_tt);//
	}
	else
	{
		gmtime_r(&_t, &_tt);//格林时间
	}
#endif
	unsigned int curdt = _tt.tm_hour * 3600 + _tt.tm_min * 60 + _tt.tm_sec;
	return curdt;
};

std::string pyfree::getCurDayStr(std::string format, bool localf)
{
	time_t _t = time(NULL);
	struct tm _tt;
#ifdef WIN32
	if (localf)
	{
		localtime_s(&_tt, &_t);//系统本地时间
	}
	else
	{
		gmtime_s(&_tt, &_t);//格林时间
	}
#else
	if (localf)
	{
		localtime_r(&_t, &_tt);//
	}
	else
	{
		gmtime_r(&_t, &_tt);//格林时间
	}
#endif
	_tt.tm_year += 1900;
	_tt.tm_mon += 1;
	char buf[64] = { 0 };
	sprintf(buf, format.c_str()
		, _tt.tm_year, _tt.tm_mon, _tt.tm_mday);
	return std::string(buf);
};

unsigned int pyfree::getUsec()
{
	unsigned int ret = 0;
	try {
#ifdef WIN32
		struct timeb curT;
		ftime(&curT);
		ret = static_cast<unsigned int>(curT.millitm);
#else
		struct timeval curT;
		gettimeofday(&curT, NULL);
		ret = static_cast<unsigned int>(curT.tv_usec % 1000);
#endif
	}
	catch(...){
		Print_WARN("get usec fail!\n");
	}
	return ret;
}

unsigned long long pyfree::getClockTime()
{
	unsigned long long ret = 0;
	try {
#ifdef WIN32
		ret = static_cast<unsigned long long>(clock());
#else
		ret = static_cast<unsigned long long>(clock()/1000);//保持与win同性质,只计算到ms
		//struct timeval curT;
		//gettimeofday(&curT, NULL);
		//ret = static_cast<unsigned long long>(curT.tv_usec);
#endif
	}
	catch (...) {
		Print_WARN("clock() fail!\n");
	}
	return ret;
};

std::string pyfree::getDateTime(unsigned int sec, int msec)
{
	time_t _t = (time_t)sec;
	struct tm _tt;
#ifdef WIN32
	localtime_s(&_tt, &_t);
#else
	localtime_r(&_t, &_tt);
#endif
	_tt.tm_year += 1900;
	_tt.tm_mon += 1;
	char buf[32] = { 0 };
	if(msec>=0)
	{
		sprintf(buf, "%04d-%02d-%02d %02d:%02d:%02d.%03d"
			, _tt.tm_year, _tt.tm_mon, _tt.tm_mday, _tt.tm_hour, _tt.tm_min, _tt.tm_sec, msec);
	}else{
		sprintf(buf, "%04d-%02d-%02d %02d:%02d:%02d"
			, _tt.tm_year, _tt.tm_mon, _tt.tm_mday, _tt.tm_hour, _tt.tm_min, _tt.tm_sec);
	}
	return std::string(buf);
}

void pyfree::getCurTime(unsigned int &s, unsigned int &ms)
{
	try {
#ifdef WIN32
		struct timeb curT;
		ftime(&curT);
		s = static_cast<unsigned int>(curT.time);
		ms = static_cast<unsigned int>(curT.millitm);
#else
		struct timeval curT;
		gettimeofday(&curT, NULL);
		s = static_cast<unsigned int>(curT.tv_sec);
		ms = static_cast<unsigned int>(curT.tv_usec % 1000);
#endif
	}
	catch (...) {
		Print_WARN("get usec fail!\n");
	}
}

unsigned long pyfree::getCurMsTime()
{
	unsigned long cur_ms = 0;
	unsigned int s=0,ms = 0;
	getCurTime(s,ms);
	cur_ms = static_cast<unsigned long>(s)*1000+static_cast<unsigned long>(ms);
	return cur_ms;
}

bool pyfree::isMapTime(unsigned int startTime,unsigned int endTime)
{
	time_t _t = time(NULL);
	struct tm _tt;
#ifdef WIN32
	localtime_s(&_tt, &_t);//系统本地时间
#else
	localtime_r(&_t, &_tt);//
#endif
	unsigned int curMinT = _tt.tm_hour * 60 + _tt.tm_min;
	if (startTime <= endTime) 
	{
		if (curMinT >= startTime&&curMinT < endTime)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else 
	{
		if (curMinT >= startTime || curMinT < endTime)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
};