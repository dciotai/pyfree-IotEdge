#!/bin/bash
#@pyfree 2020-06-06,py8105@163.com
#创建程序目录
#run for root
#定义程序全局路径变量
#程序根目录
dir='/usr/local/waringPush'
curdir=$(pwd)

if [ ! -d $dir ]; then
    mkdir -p $dir
fi
#
echo "appdir="$dir
echo "curdir="$curdir
#复制程序文件到程序目录
/bin/cp -f {pyfree-waringPush,SWL,*.xml} $dir
#
ls $dir
#
if [ ! -f "$dir/waringPushStart.sh" ];then
#创建程序启动脚本gather
cat > $dir/waringPushStart.sh << eof
#!/bin/bash
pid=\`ps -ef | grep "pyfree-waringPush*" | grep -v "grep" | wc -l\`
if [ \$pid -eq 0 ];then
    cd $dir
	echo "The waringPush server will be start"
    nohup ./pyfree-waringPush & > /dev/null
else
    echo "The waringPush server is alreadly running"
fi 
eof
fi

if [ ! -f "$dir/waringPushStop.sh" ];then
#创建程序关闭脚本gather
cat > $dir/waringPushStop.sh << eof
#!/bin/bash
pid=\`ps -ef | grep "pyfree-waringPush*" | grep -v "grep" | wc -l\`
if [ \$pid -ne 0 ];then
	echo "The waringPush server will be stop"
    killall -9 pyfree-waringPush
else
	echo "The waringPush server is stoping, don't stop it"
fi 
eof
fi

#给服务启动脚本添加执行权限
chmod o+x $dir/{waringPushStart.sh,waringPushStop.sh}

if [ ! -f "/usr/lib/systemd/system/pyfreeWaringPush.service" ];then
#创建程序启动文件到centos7服务启动路径/usr/lib/systemd/system
cat > /usr/lib/systemd/system/pyfreeWaringPush.service <<eof
[Unit]
Description=pyfree-waringPush
After=syslog.target network.target remote-fs.target nss-lookup.target
[Service]
Type=forking
ExecStart=$dir/waringPushStart.sh
ExecReload=
ExecStop=$dir/waringPushStop.sh
PrivateTmp=true
[Install]
WantedBy=multi-user.target
eof
fi

#给gather生成liscense文件sn.txt, 如果是32位系统，修改为SWL_x32.exe 
#0表示网卡，1表示磁盘； 22表示生成sn.txt文件，这里选择网卡，生成sn.txt liscense文件
cd $dir
./SWL 0 22
cd $curdir

#设置开机启动gather服务
systemctl daemon-reload
chmod o-x /usr/lib/systemd/system/pyfreeWaringPush.service
systemctl enable pyfreeWaringPush.service
#查看服务是否开机启动：
#systemctl is-enabled pyfreeWaringPush.service
#设置开机时禁用服务
#systemctl disable pyfreeWaringPush.service
#启动gather服务
#systemctl start pyfreeWaringPush.service

#停止pcsserver服务
#systemctl stop pyfreeWaringPush.service
