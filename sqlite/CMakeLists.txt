# CMake 最低版本号要求
cmake_minimum_required (VERSION 2.8)
# 项目信息
project (csqlite)
#
if(WIN32)
    message(STATUS "windows compiling...")
    add_definitions(-D_PLATFORM_IS_WINDOWS_)
	set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} /MT")
	set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} /MTd")
else(WIN32)
    message(STATUS "linux compiling...")
    add_definitions( -D_PLATFORM_IS_LINUX_)
endif(WIN32)
#
set(LIBRARY_OUTPUT_PATH  ${PROJECT_SOURCE_DIR}/lib)
SET(sqlite_h
    ${PROJECT_SOURCE_DIR}/include/sqlite3ext.h
    ${PROJECT_SOURCE_DIR}/include/sqlite3.h
    ${PROJECT_SOURCE_DIR}/include/CSQLite.h
)

SET(sqlite_cpp
    ${PROJECT_SOURCE_DIR}/src/sqlite3.c
    ${PROJECT_SOURCE_DIR}/src/CSQLite.cpp
)
  
#头文件目录
include_directories(${PROJECT_SOURCE_DIR}/include())
#
# 指定生成目标
add_library(csqlite STATIC ${sqlite_h} ${sqlite_cpp})
