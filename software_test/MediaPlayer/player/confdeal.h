#ifndef _CONFDEAL_H_
#define _CONFDEAL_H_
/*
*程序运行参数，在文本中预配置，不预先配置的选项，写入默认值
*/
#include <QString>
#include <QStringList>
#include <QMap>

QT_BEGIN_NAMESPACE
class QSettings;
QT_END_NAMESPACE

class ConfDeal
{
public:
	static ConfDeal* getInstance(bool _iniSetf=true);
	static void Destroy();
	~ConfDeal();

	QStringList getListFromStr(QString _str);
	//
	QString getappDir();
	QString getpathDiv();
	//General
    QString getMediaDir();
    int getPlayModel();
    int getFullScreen();
    int getAutoPlay();
	int getListenPort();
private:
	void init();
	void initconf();
	void readconfQt();
	void printfconf();
	void initSet();

	ConfDeal(bool _iniSetf);
	ConfDeal(const ConfDeal& ) {} // copy constructor
	ConfDeal& operator=(const ConfDeal& ) { return *this; } // assignment operator
private:
	static ConfDeal* instance;
	bool iniSetf;

	QSettings* settings;

	QString appDir;
	QString divPath;
};

#endif //_CONFDEAL_H_
