#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _TRANSMIT_CHANNEL_H_
#define _TRANSMIT_CHANNEL_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       :  tchannel.h
  *File Mark       : 
  *Summary         : 
  *向客户端发送数据前先进行协议编码并序列化
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include "conf_channel.h"
#include "datadef.h"
#include "queuedata.h"
#include "queuedata_single.h"

class ReceiveData;
class luadisp;

class TranChannel
{
public:
	TranChannel(int tid_);
	virtual ~TranChannel(void);
	
	/**
	 * 添加采集通道推送给转发通道的业务数据
	 * @param it {ChannelToTransmit} 业务数据
	 * @return {void }
	 */
	void add(ChannelToTransmit it);
protected:
	/**
	 * 根据lua脚本文件进行属于编码,按约定协议推送给上层应用
	 * @param buf {char*} 报文指针
	 * @param size {int} 缓存空间大小
	 * @return {int } 报文长度
	 */
	int getBufferTran(unsigned char * _buf, int size);
	/**
	 * 根据lua脚本文件进行属于编码,按约定协议推送给上层应用,2020-05-29,从getBufferTran独立出来
	 * @param buf {char*} 报文指针
	 * @param it {ChannelToTransmit} 下控信息
	 * @return {int } 报文长度
	 */
	int getDownControlByLua(unsigned char *buf,int size, ChannelToTransmit it);
	/**
	 * 根据lua脚本文件进行属于编码,按约定协议推送给上层应用,2020-05-29因串口转发增加,目前仅在串口转发时调用
	 * @param buf {char*} 报文指针
	 * @param info_desc {string} 信息点态势集,格式"id,val;id,val;..."
	 * @return {int } 报文长度
	 */
	int getResponseByLua(unsigned char *buf, int size
		, std::string info_desc,unsigned int respType=1, unsigned long taskID=0L,bool changeF=false);
	/**
	 * 根据lua脚本文件进行上层应用下发报文解析
	 * @param buf {char*} 报文指针
	 * @param len {int} 报文长度
	 * @return {int } <0时异常
	 */
	int AddFrameTran(const unsigned char *buf, int len);
	/**
	 * 根据lua脚本文件进行上层应用下发报文解析,2020-05-29,为实现串口响应从AddFrameTran独立出来
	 * @param buf {char*} 报文指针
	 * @param len {int} 报文长度
	 * @param trans_ {QueueData&} 下控信息
	 * @return {int } <0时异常
	 */
	int AddFrameByLua(const unsigned char *buf, int len,QueueData<TransmitToGChannel> &trans_);
private:
	/**
	 * 初始化脚本接口实例
	 * @return {void } 
	 */
	void initLua();
	/**
	 * 析构脚本接口实例
	 * @return {void } 
	 */
	void releaseLua();
	/**
	 * 重新加载解析脚本,将总召信息解析并缓存,转发接口暂时无总召需求,待实现
	 * @return {void } 
	 */
	void reSetlua()
	{
		
	};

protected:
	//转发端的属信息集
	pyfree::Transmit tatts;
	//缓存转发端获取的业务数据
	QueueData<ChannelCmd> *ReadDatas;
	//缓存待写入转发端的业务数据
	QueueData<ChannelToTransmit> *WriteDataS;
	//
	//写入采集端口的数据缓存,由信道管理线程进行业务中转,需要在本端口做业务数据转换处理
	QueueDataSingle<TransmitToGChannel> *ToGchannelDatas;
private:
	//协议配置,制定解析脚本
	pyfree::ProtocolDef protodefs;
	/*
	*明确是在内部解析协议,还是调用脚本解析协议
	*@protoType=1,调用lua脚本解析
	*@protoType=2,内部默认的单点的数据转发监控服务
	*@protoType=3,内部默认的多点的数据转发监控服务
	*/
	int protoType;
	//通信数据解析脚本
	luadisp *luaScript;
};
#endif
