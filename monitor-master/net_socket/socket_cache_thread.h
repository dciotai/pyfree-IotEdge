#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _SOCKET_CACHE_THREAD_H_
#define _SOCKET_CACHE_THREAD_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : socket_cache_thread.h
  *File Mark       :
  *Summary         : 
  *来自采控服务缓存数据的独立处理线程,当前主要是实现数据帧解析
  *
  *Current Version : 1.00
  *Author          : PengYong
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include "lib_acl.h"
#include "acl_cpp/lib_acl.hpp"

#ifdef linux
#include <string>
#include<iostream>
#endif

#include "datadef.h"
#include "queuedata_single.h"

class BusinessDef;

class SocketCacheThread : public acl::thread
{
public:
	SocketCacheThread(void);
	~SocketCacheThread(void);
  //
	void* run();
  /**
	 * 帧数据解析
   * @param link {string } 采集端的网络地址
   * @param buf {congst uchar* } 帧数据
   * @param len {int } 帧大小
	 * @return {int} <0异常,>0成功
	 */
	int AddFrame(const std::string link, const unsigned char *buf, int len);
private:
	bool running;     //线程运行标记
	BusinessDef *ptr_bdef;  //业务信息,单体类实例
	QueueDataSingle<DataFromChannel> *socket_cache_queue; //来自各个采集端的缓存数据
};
#endif
