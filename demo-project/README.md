# pyfree-IotEdge demo 使用说明

### 目录结构 
* bagather  
    模拟下级采集软件,进行采集级联测试时使用  
* client_test  
    qt开发的本地可视化终端,其作为客户端通过tcp/ip-sokcet连接到调度软件  
    其数据展示为二层树结构展示模式,第一层级为设备，第二层级为信息点  
* docker_test  
    采集、调度软件的基于centos:7的docker样例  
* gather  
    采集软件demo,本demo可以与作为上层应用与bagather进行级联,是数据汇聚  
* historyCurve_test  
    调度软件记录数据(sqlite)的可视化展示工具  
* monitor  
    调度软件demo,与作为服务端与采集软件通信,作为服务端与本地客户端client_test通信,作为客户端与阿里云平台、其他mqtt中间件通信  
* player_test  
    基于qt自带demo-player改造的媒体播放器,用于测试采集软件的网路采集,其作服务端与采集软件通信  
* sysmgr  
    服务管理软件,对采集、调度服务进行服务态势控制和相关配置文件上传、下载等管理  
* record  
    记录软件,调度服务通过udp通信推送数据本软件    
* tool  
    服务于测试demo的工具包：  
    (1)"7z.zip"用于win下服务管理软件与阿里云物联网平台实现采集软件、调度软件的固件升级时使用  
    (2)"mbslave.exe"用于模拟modbus从站终端,测试采集软件的串口通信,bagather样例作为主站端与其实现串口通信 
    (3)"VSPD虚拟串口.zip"该工具包用于win下创建虚拟串口对,为"mbslave.exe"和bagather样例构建串口通信的物理通道    

### 部署说明  
* demo结构:  
    ![demo结构示意图](/res/for_doc/demo_construct.PNG)  

    * 全局说明:  
        配置文件格式均为utf-8格式,因此在程序内流转的字段多为utf-8格式  

        
* 采集软件(gather)
    * 配置说明:   
        采集软件有三个重要的配置文件appconf.xml、gather.xml、gmap.xml,appconf.xml主要配置程序运行参数信息,gather.xml配置采集信道信息,gmap.xml主要配置采集点到转发点的映射关。  
        appconf.xml、gmap.xml相对简单,查看其文件内的说明就可理解,这里主要讲述gather.xml配置时的一些注意实现：
        (1)转发(transmit)索引(id)、串口(serialport)索引(id)、网口(netport)索引(id)、网口(protocol)索引(id)采集(gather)索引(id)需要保持本类型内唯一；
        (2)采集信道的为串口时(channeltype=1), 其name参数是有实际用途的,需要正确配置,win一般是COM*格式,linux下为/dev/ttyS*格式;  
        (3)本机在测试linux串口时,主要是在vmware虚拟工具安装了centos7进行测试,具体实现为先采用"tool/VSPD虚拟串口.zip"串口工具安装并创建一个串口对COM4<->COM5,在虚拟机关闭时，进入虚拟机设置页面，添加串行端口,指定使用物理串行端口，选择COM4(前提需要创建虚拟串口对),更详细的可以参考gather-master源码目录下的说明文档;    
        (4)采集信道的为网口级联时(channeltype=3),各个信息点的ip是有实际用途的,ip需要指定是来自哪个下级软件软件,更形象的信息可以参考gather/gather.xml配置样例;  
        (5)采集信道的为网口级联时(channeltype=3),虽然其作为服务端，但是其对应的网络参数配置信息的ip必须配置指定网口网络地址,否则当前版本程序启动会异常退出;  
        (6)采集信道配置参数的更多描述,请详见gather/gather.xml配置样例;
        (7)数据流转,采集信道读取到数据通过脚本解析得到(点,值)数据对集,采集数据对通过映射关系转为转发数据对,再通过脚本解析生成通信协议推送给上层应用端。    

    * demo通信说明:  
        bgather与"mbslave.exe"进行串口通信,实现modbus数据采集,报文解析调用modbus01.lua脚本;  
        bgather与gather进行网络通信,实现级联采集,报文解析调用tcpPCS.lua脚本;  
        player_test与gather进行网络通信,实现网络采集,报文解析调用tcpPlay.lua脚本;  
        gather与monitor进行网络通信,实现数据转发上层应用,报文解析调用tcp03.lua脚本;   

    * 安装使用:  
        win安装: 管理人员启动命令行工具,进入程序目录,运行pyfree-gather.exe install完成安装    
        win卸载: 任务管理器或服务管理页面,停止pyfreeGather服务,运行pyfree-gather.exe uninstall完成卸载
        win更新: 任务管理器或服务管理页面,停止pyfreeGather服务,拷贝pyfree-gather.exe到程序目录覆盖完成更新  

        linux安装: root用户,运行install_linux.sh完成安装
        linxu卸载: 目前暂无提供卸载脚本,处理过程:先停止服务,systemctl stop pyfreeGather.service,禁止开机启动systemctl disable pyfreeGather.service,后续删除相关文件即可,具体参看install_linux.sh脚本  
        linux更新: 先停止服务,systemctl stop pyfreeGather.service,拷贝pyfree-gather到程序目录覆盖更新  

    * 注意事项:  
        非全本机部署时,需要注意网络防火墙对相关端口的通信屏蔽,尤其是linux下,需要配置相关端口通过,如果仅仅是测试也可以直接关闭防火墙:systemctl stop firewalld.service  
        软件启动进行校验，其校验码是由专项开发的SWL(.exe)校验码生成软件基于本部署硬件资源生成的验证码并写入一个sn.txt文件，约束本软件仅能在该硬件环境下使用,具体参数效用,参看swLicense/src/main.cpp源码,如果需要商业应用请以此改造  

* 调度软件(monitor)    
    * 配置说明:  
        调度软件重要的配置文件appconf.xml、dev.xml、pmap.xml、plan.xml、alarm.xml:appconf.xml配置了程序运行参数;dev.xml配置了业务数据;pmap.xml配置了采集数据到业务数据的映射关系;plan.xml、alarm.xml内容格式类似,前者配置任务调度策略,后者配置告警策略。  
        (1)appconf.xml主要是配置各个功能模块开闭以及模块涉及参数,具体看参看monitor/appconf.xml文件;  
        (2)dev.xml配置了业务信息,采用设备-信息点的二层树结构,其中设备编号唯一,信息点编号在设备内唯一,设备可以是实际设备、虚拟设备、组合设备等,信息点可以实际采集点也可以是虚拟点,实际点通常在pmap.xml与采集软件过来的信息点构建映射关系。  
        (3)pmap.xml配置配置映射关系,需要指定采集软件所在的网络地址,目前仅支持每个信息点独立设置,具体参看monitor/pmap.xml文件样例。  
        (4)plan.xml配置文件实现任务调度,程序运行过程中不断巡检配置的运行条件,满足条件时进行调度操作(设置、查询等)。需要注意任务编号唯一性,planType用来标记任务类型(具体参看monitor/plan.xml),不同任务类型其条件配置不一样。
        (5)alarm.xml配置告警策略,告警策略与任务策略基本一致,不同的是执行指令,任务配置执行对信息点的查询、设值,而告警策略主要执行告警输出。  

    * demo通信说明: 
        gather与monitor进行tcp/ip-socket通信,monitor作为服务端,获取到各个采集软件推送的采集数据,也可进行远程设值下控。  
        client_test与monitor进行tcp/ip-socket通信,monitor作为服务端,客户端展示的业务数据结构就是dev.xml配置的实时业务数据。     
        monitor与阿里云物联网平台通信,需要在平台配置一个网关产品、若干设备产品,并以此创建设备实例，并设置设备实例为网关设备的子设备。在appconf.xml配置网关设备三元组信息,在dev.xml配置设备实例的三元组信息以及功能定义标识。更具体说明参考monitor-master的说明文档。      
        monitor作为客户端与第三方平台tcp/ip-socket通信,推送和接收实时数据,需要在appconf.xml配置相关参数与开启功能。    
        monitor作为客户端与mqtt服务通信,推送和接收实时数据,需要在appconf.xml配置相关参数与开启功能。  
        monitor与聚合天气服务通信,获取天气信息,需要在appconf.xml配置相关参数与开启功能。需要阿里云市场购买聚合天气服务流量包。
        monitor作为udp发送方,与记录软件、告警推送通信，实现数据的记录、告警信息发布。    

    * 安装使用:  
        win安装: 管理人员启动命令行工具,进入程序目录,运行pyfree-monitor.exe install完成安装    
        win卸载: 任务管理器或服务管理页面,停止pyfreeMonitor服务,运行pyfree-monitor.exe uninstall完成卸载
        win更新: 任务管理器或服务管理页面,停止pyfreeMonitor服务,拷贝pyfree-monitor.exe到程序目录覆盖完成更新  

        linux安装: root用户,运行install_linux.sh完成安装
        linxu卸载: 目前暂无提供卸载脚本,处理过程:先停止服务,systemctl stop pyfreeMonitor.service,禁止开机启动systemctl disable pyfreeMonitor.service,后续删除相关文件即可,具体参看install_linux.sh脚本  
        linux更新: 先停止服务,systemctl stop pyfreeMonitor.service,拷贝pyfree-monitor到程序目录覆盖更新  

    * 注意事项:  
        非全本机部署时,需要注意网络防火墙对相关端口的通信屏蔽,尤其是linux下,需要配置相关端口通过,如果仅仅是测试也可以直接关闭防火墙:systemctl stop firewalld.service  
        软件启动进行校验，其校验码是由专项开发的SWL(.exe)校验码生成软件基于本部署硬件资源生成的验证码并写入一个sn.txt文件，约束本软件仅能在该硬件环境下使用,具体参数效用,参看swLicense/src/main.cpp源码,如果需要商业应用请以此改造   

* 记录软件(record) 

    * 配置说明:  
        记录软件重要的配置文件appconf.xml配置了程序运行参数,涉及udp通信、sqlite记录的配置信息。  
        (1)appconf.xml主要是配置udp通信和sqlite记录涉及参数,具体看参看record/appconf.xml文件;    

    * demo通信说明: 
        record与monitor进行udp通信,monitor作为发送方,record作为接收方,采用自定义协议格式通信。    

    * 安装使用:  
        win安装: 管理人员启动命令行工具,进入程序目录,运行pyfree-record.exe install完成安装    
        win卸载: 任务管理器或服务管理页面,停止pyfreeRecord服务,运行pyfree-record.exe uninstall完成卸载
        win更新: 任务管理器或服务管理页面,停止pyfreeRecord服务,拷贝pyfree-record.exe到程序目录覆盖完成更新  

        linux安装: root用户,运行install_linux.sh完成安装
        linxu卸载: 目前暂无提供卸载脚本,处理过程:先停止服务,systemctl stop pyfreeRecord.service,禁止开机启动systemctl disable pyfreeRecord.service,后续删除相关文件即可,具体参看install_linux.sh脚本  
        linux更新: 先停止服务,systemctl stop pyfreeRecord.service,拷贝pyfree-record到程序目录覆盖更新  

    * 注意事项:  
        非全本机部署时,需要注意网络防火墙对相关端口的通信屏蔽,尤其是linux下,需要配置相关端口通过,如果仅仅是测试也可以直接关闭防火墙:systemctl stop firewalld.service  
        软件启动进行校验，其校验码是由专项开发的SWL(.exe)校验码生成软件基于本部署硬件资源生成的验证码并写入一个sn.txt文件，约束本软件仅能在该硬件环境下使用,具体参数效用,参看swLicense/src/main.cpp源码,如果需要商业应用请以此改造  

* 告警推送软件(waringpush) 

    * 配置说明:  
        记录软件重要的配置文件appconf.xml配置了程序运行参数,涉及udp通信、短信、邮件告警的配置信息。  
        (1)appconf.xml主要是配置udp通信、告警推送接口涉及参数,具体看参看waringpush/appconf.xml文件;    

    * demo通信说明: 
        waringpush与monitor进行udp通信,monitor作为发送方,record作为接收方,采用json内容格式通信。    

    * 安装使用:  
        win安装: 管理人员启动命令行工具,进入程序目录,运行pyfree-waringPush.exe install完成安装    
        win卸载: 任务管理器或服务管理页面,停止pyfreeWaringPush服务,运行pyfree-waringPush.exe uninstall完成卸载
        win更新: 任务管理器或服务管理页面,停止pyfreeWaringPush服务,拷贝pyfree-waringPush.exe到程序目录覆盖完成更新  

        linux安装: root用户,运行install_linux.sh完成安装
        linxu卸载: 目前暂无提供卸载脚本,处理过程:先停止服务,systemctl stop pyfreeWaringPush.service,禁止开机启动systemctl disable pyfreeWaringPush.service,后续删除相关文件即可,具体参看install_linux.sh脚本  
        linux更新: 先停止服务,systemctl stop pyfreeWaringPush.service,拷贝pyfree-record到程序目录覆盖更新  

    * 注意事项:  
        非全本机部署时,需要注意网络防火墙对相关端口的通信屏蔽,尤其是linux下,需要配置相关端口通过,如果仅仅是测试也可以直接关闭防火墙:systemctl stop firewalld.service  
        软件启动进行校验，其校验码是由专项开发的SWL(.exe)校验码生成软件基于本部署硬件资源生成的验证码并写入一个sn.txt文件，约束本软件仅能在该硬件环境下使用,具体参数效用,参看swLicense/src/main.cpp源码,如果需要商业应用请以此改造 

* 管理软件(sysmgr)    
    * 配置说明:  
        服务管理软件主要配置文件为appconf.xml、svc.xml,前置为程序运行参数配置文件,后者为服务相关信息配置文件。  
        (1)appconf.xml主要是配置阿里云物联网平台接口相关信息,涵盖两个方面,即态势推送相关和固件升级相关。更详细请参看实例文件和源码的说明文档。  
        (2)svc.xml文件主要配置需要监控与管理的服务相关信息,样例里配置采集、调度两个服务,有其他服务需要处置类似配置即可。但需要在阿里云物联网平台网关产品中添加新的功能定义适配。  

    * demo通信说明:  
        web浏览器与sysmgr进行http通信,实现在本地web终端可视化管理服务,暂时只有服务启停和配置文件上传下载等简单功能。  
        sysmgr与阿里云物联网平台通信,实现在平台监控服务态势和固件升级。  

    * 安装使用:  
        win安装: 管理人员启动命令行工具,进入程序目录,运行pyfree-sysmgr.exe install完成安装    
        win卸载: 任务管理器或服务管理页面,停止pyfreeMgr服务,运行pyfree-sysmgr.exe uninstall完成卸载
        win更新: 任务管理器或服务管理页面,停止pyfreeMgr服务,拷贝pyfree-sysmgr.exe到程序目录覆盖完成更新  

        linux安装: root用户,运行install_linux.sh完成安装
        linxu卸载: 目前暂无提供卸载脚本,处理过程:先停止服务,systemctl stop pyfreeMgr.service,禁止开机启动systemctl disable pyfreeMgr.service,后续删除相关文件即可,具体参看install_linux.sh脚本  
        linux更新: 先停止服务,systemctl stop pyfreeMgr.service,拷贝pyfree-sysmgr到程序目录覆盖更新  

    * 注意事项:  
        非全本机部署时,需要注意网络防火墙对相关端口的通信屏蔽,尤其是linux下,需要配置相关端口通过,如果仅仅是测试也可以直接关闭防火墙:systemctl stop firewalld.service  
        软件启动进行校验，其校验码是由专项开发的SWL(.exe)校验码生成软件基于本部署硬件资源生成的验证码并写入一个sn.txt文件，约束本软件仅能在该硬件环境下使用,具体参数效用,参看swLicense/src/main.cpp源码,如果需要商业应用请以此改造    

* 本地客户端(client_test)  
    * 该客户端为瘦客户端,业务结构数据在重新连接上调度服务后获取生成层级结构,后续调度软件不断推送实时数据,也可以向调度软件发送下控指令。需要在其配置文件conf.ini配置调度软件所在网络地址和其侦听端口。 另网络地址也可以在终端页面设置,但暂时不支持端口设置。  

    * 终端采用树结构暂时设备状态信息，设备是通过配置的虚拟设备，可将实体设备的采集点进行映射到虚拟设备下，具体操作：  
        1）点击设备项的右边”>”可以展开设备下属信息点集，标题栏替换为该设备名称，列表展示所有信息点、时间、值，更新时间如果与当前时间差值超出阀值，将红色状态表示，状态值（0，1）将转换为枚举名称，遥测值为数据值；  
        2）信息点列表中，状态项（遥信量）可以通过双击实现变位控制，资源项（遥测量）双击会弹出输入框，输入需要的值进行设置并回车确认；  
        3）标题栏展示设备名是，点击”<”可以返回设备列表。  
        4）点击链接符号可以设置ip地址，回车确认  

* 播放软件(player_test)
    * 用于测试采集软件网络采集使用, 其作为服务端,其侦听端口和网络地址可以在conf.ini配置。另外建议在配置文件指定视频媒体目录。具体网络通信协议参看源码(software_test/MediaPlayer/player/socketServer.cpp)更容易了解清楚,亦可查看tcpplay.lua脚本。 

    * 需要LAVFilters媒体解析库的支持,demo提供的为win64和player.exe配套,需要win32支持的自行编译和下载32bit的LAVFilters。     

* modbus模拟终端(tool/mbslave.exe)    
    * 该终端模拟modbus从站侧,需要通过"VSPD虚拟串口.zip"工具构建虚拟串口对,注意打开虚拟串口对的其中一个,并确保其串口参数与bgahter的配置串口参数一致。  
	
* 数据展示软件(historyCurve_test)  
	* 该软件需要在conf.ini配置调度软件数据记录目录即可启动软件查看历史数据信息。  