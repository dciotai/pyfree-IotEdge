﻿--应用脚本
--脚本语言:LUA (http://www.lua.org )
--项目应用:pyfree项目
--脚本版本:1.0
-----------------------------------------------------------------------------------------------------------------------------------------
function getTotalCallCMD()
	allcall = ""
	return allcall,0
end

function DEC_HEX(IN)
    local B,K,OUT,I,D=16,"0123456789ABCDEF","",0
    while IN>0 do
        I=I+1
        IN,D=math.floor(IN/B),math.mod(IN,B)+1
        OUT=string.sub(K,D,D)..OUT
    end
    return OUT
end

function num2hex(num)
    local hexstr = '0123456789abcdef'
    local s = ''
    while num > 0 do
        local mod = math.fmod(num, 16)
        s = string.sub(hexstr, mod+1, mod+1) .. s
        num = math.floor(num / 16)
    end
    if s == '' then s = '0' end
    return s
end

function getDownControl(exetype, ptype, addr, val)
	controlCmd = "NULL"
	check = 2
	print(string.format("%d,%d,%d,%.3f",exetype,ptype,addr,val))
	--print("..1..")
	if 1==exetype then
		--print("..1..")
		controlCmd = ""
	elseif 2==exetype then
		--print("..2..")
		intStr = string.format("%08X",addr)
		--print(intStr)
		valStr = string.format("%08X",val)
		--print(valStr)
		intStr_ = string.sub( intStr, 7, 8 )..string.sub( intStr, 5, 6 )..string.sub( intStr, 3, 4 )..string.sub( intStr, 1, 2 )
		valStr_ = string.sub( valStr, 7, 8 )..string.sub( valStr, 5, 6 )..string.sub( valStr, 3, 4 )..string.sub( valStr, 1, 2 )
		controlCmd = "F3".."0B00"..intStr_..valStr_.."FF"
		--print(controlCmd)
	else 
		--print("..3..")
		lenStr = string.format("%04X",12)
		--print(lenStr)
		intStr = string.format("%08X",addr)
		--print(intStr)
		ptypeStr = string.format("%02X",ptype)
		--print(ptypeStr)
		valStr = string.format("%08X",val)
		--print(valStr)
		lenStr_ = string.sub( lenStr, 3, 4 )..string.sub( lenStr, 1, 2 )
		intStr_ = string.sub( intStr, 7, 8 )..string.sub( intStr, 5, 6 )..string.sub( intStr, 3, 4 )..string.sub( intStr, 1, 2 )
		valStr_ = string.sub( valStr, 7, 8 )..string.sub( valStr, 5, 6 )..string.sub( valStr, 3, 4 )..string.sub( valStr, 1, 2 )
		controlCmd = "F7"..lenStr_..intStr_..ptypeStr..valStr_.."FF"
		print(controlCmd)
	end
	--print("..4..")
	return controlCmd,check
end

function hex2string(hex)
	local str, n = hex:gsub("(%x%x)[ ]?", function (word)
	return string.char(tonumber(word, 16))
	end)
	return str
end

function getReciveIDVAL(up_cmd,down_cmd)
	stra = up_cmd
	--print(stra)
	recs = ""
	local exeType = 0
	local pType = 0
	--print("...1...\n")
	if string.sub( stra, 1, 1 )~="F" then
		return recs,0
	end
	if string.sub( stra, 1, 2 )~="F5" then
		exeType = 1
	end
	if string.sub( stra, 1, 2 )~="F6" then
		exeType = 2
	end
	--print("...2...\n")
	if string.len(stra)<13 then
		return recs,0
	end
	--print("...3...\n")
	lLen = string.sub( stra, 3, 4 )
	hLen = string.sub( stra, 5, 6 )
	local dlen = tonumber("0x"..lLen)+16*tonumber("0x"..hLen)
	--print(dlen)
	if dlen<=0 then
		return recs,0
	end
	--print("...4...\n")
	size = 0;
	id04 = string.sub( stra, 7, 8 )
	id03 = string.sub( stra, 9, 10 )
	id02 = string.sub( stra, 11, 12 )
	id01 = string.sub( stra, 13, 14 )
	--print(id04..id03..id02..id01)
	local idx = tonumber("0X"..id04)+16*tonumber("0X"..id03)+256*tonumber("0X"..id02)+4096*tonumber("0X"..id01)
	--print(idx)	
	--print("...5...\n")
	ptypeStr = string.sub( stra, 15, 16 )
	pType = tonumber("0x"..ptypeStr)
	--print("...5...\n")
	
	val04 = string.sub( stra, 17, 18 )
	val03 = string.sub( stra, 19, 20 )
	val02 = string.sub( stra, 21, 22 )
	val01 = string.sub( stra, 23, 24 )
	--print(val04..val03..val02..val01)
	val = tonumber("0X"..val04)+16*tonumber("0X"..val03)+256*tonumber("0X"..val02)+4096*tonumber("0X"..val01)

	valmap = string.format("%d,%d,%d,%d",idx,val,pType,exeType)
	--print(valmap)
	recs = recs..valmap
	size = 1

	--print("...6...\n")
	return recs,size
end
