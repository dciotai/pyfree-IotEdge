#include "confdeal.h"

//#include <vector>
//#include <string>

#include <QSettings>
#include <QDir>
#include <QDebug>
#include <QString>
#include <QStringList>
#include <QCoreApplication>

// #include "File.h"

/*
**将字符串按指定标识分段
*/
//inline bool string_divide( std::vector<std::string> &_strlist,const std::string src,const std::string div)
//{
//	std::string _src = src;
//	std::string::size_type _pos = _src.find(div);
//	while(std::string::npos != _pos)
//	{
//		std::string _buf = "";
//		_buf = _src.substr(0,_pos);
//		_strlist.push_back(_buf);
//		_src = _src.erase(0,_pos+div.size());
//		_pos = _src.find(div.c_str());
//	}
//	if(!_src.empty()){
//		_strlist.push_back(_src);
//	}
//	return true;
//};

ConfDeal* ConfDeal::instance = NULL;
ConfDeal* ConfDeal::getInstance(bool _iniSetf)
{
    if(NULL == ConfDeal::instance)
    {
        ConfDeal::instance = new ConfDeal(_iniSetf);
    }
    return ConfDeal::instance;
};

void ConfDeal::Destroy()
{
	if(NULL!=ConfDeal::instance){
		delete ConfDeal::instance;
		ConfDeal::instance = NULL;
	}
};

ConfDeal::ConfDeal(bool _iniSetf) : iniSetf(_iniSetf)
{
	init();
};

ConfDeal::~ConfDeal()
{
};

void ConfDeal::init()
{
	initconf();
	if (iniSetf)
	{
		readconfQt();
		printfconf();
	}
};

void ConfDeal::initconf()
{
	appDir = QCoreApplication::applicationDirPath();
	divPath="/";
	#ifdef WIN32
	appDir = appDir.replace("/","\\");
	divPath="\\";
	#endif
	qDebug() << " appDir = " << appDir << "\n";
};

void ConfDeal::readconfQt()
{
#ifdef ANDROID
    QString _confFile = "/storage/emulated/0/zhsye/conf.ini";
#else
	QString _confFile = "conf.ini";
#endif
	settings=new QSettings(_confFile,QSettings::IniFormat);
	settings->setIniCodec( "UTF-8");
};

void ConfDeal::printfconf()
{
	initSet();
	QStringList groups = settings->childGroups();
	// if (groups.empty())
	// {
	// 	initSet();
	// }
	foreach(QString group,groups)
    {
    	qDebug()<<QString("....................[%1]..................").arg(group);
        settings->beginGroup(group);
        QStringList keyList=settings->childKeys();
        foreach(QString key,keyList)
        {
			qDebug()<<key<<"="<<settings->value(key).toString();
        }
        settings->endGroup();
    }
};

void ConfDeal::initSet()
{
	//
    getMediaDir();
    getPlayModel();
    getFullScreen();
    getAutoPlay();
	getListenPort();
};

QStringList ConfDeal::getListFromStr(QString _str)
{
    return _str.split(",", QString::SkipEmptyParts);
};

QString ConfDeal::getappDir()
{
	return appDir;
};

QString ConfDeal::getpathDiv()
{
	return divPath;
};

/////////////////////////////////General//////////////////////////////////////////
QString ConfDeal::getMediaDir()
{
    if (!settings->contains("General/DefMediaDir"))
    {
        #ifdef ANDROID
        settings->setValue("General/DefMediaDir",("/storage/emulated/0/zhsye"+divPath+"media"));
        #else
        settings->setValue("General/DefMediaDir",(appDir+divPath+"media"));
        #endif
        settings->setValue("General-note/DefMediaDir",QObject::tr("note-DefMediaDir"));
    }
    return settings->value("General/DefMediaDir").toString();
};

int ConfDeal::getPlayModel()
{
    if (!settings->contains("General/playModel"))
    {
        settings->setValue("General/playModel",4);
        settings->setValue("General-note/playModel",QObject::tr("note-playModel"));
    }
    return settings->value("General/playModel").toInt();
};

int ConfDeal::getFullScreen()
{
    if (!settings->contains("General/fullScreen"))
    {
        settings->setValue("General/fullScreen",0);
        settings->setValue("General-note/fullScreen",QObject::tr("note-fullScreen"));
    }
    return settings->value("General/fullScreen").toInt();
};

int ConfDeal::getAutoPlay()
{
    if (!settings->contains("General/autoPlay"))
    {
        settings->setValue("General/autoPlay",1);
        settings->setValue("General-note/autoPlay",QObject::tr("note-autoPlay"));
    }
    return settings->value("General/autoPlay").toInt();
};

int ConfDeal::getListenPort()
{
	if (!settings->contains("General/ListenPort"))
	{
        settings->setValue("General/ListenPort",10088);
		settings->setValue("General-note/ListenPort",QObject::tr("note-ListenPort"));
	}
	return settings->value("General/ListenPort").toInt();
};
