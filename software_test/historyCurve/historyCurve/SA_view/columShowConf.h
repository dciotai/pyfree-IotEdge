#ifndef COLUMSHOWCONF_HPP
#define COLUMSHOWCONF_HPP

#include <QDialog>
#include <QMap>
#include <QList>

QT_BEGIN_NAMESPACE
class QWidget;
class QMouseEvent;
class QPushButton;
class QCheckBox;
QT_END_NAMESPACE

struct ColumnShow
{
	QString _colDesc;
	bool 	_f;
};

class ColumShowConf : public QDialog
{
	Q_OBJECT
public:
	ColumShowConf(QList<ColumnShow> _list,QWidget * parent = 0,Qt::WindowFlags f = 0);
	~ColumShowConf();
protected:
	// void mousePressEvent ( QMouseEvent * event );
signals:
	void sendColumShow(QMap<int,bool> _list);
private slots:
	void setHideColum();
private:
	void win_init();
	void data_init(QList<ColumnShow> _list);
private:
	QPushButton *okButton;
    QPushButton *cancelButton;
    QList<QCheckBox*> _checklist;
};
#endif //COLUMSHOWCONF_HPP