#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef _FILE_H_
#define _FILE_H_
/***********************************************************************
  *Copyright 2020-03-06, pyfree
  *
  *File Name       : File.h
  *File Mark       : 
  *Summary         : 
  *文本读写函数类
  *
  *Current Version : 1.00
  *Author          : pyfree
  *FinishDate      :
  *
  *Replace Version :
  *Author          :
  *FinishDate      :

 ************************************************************************/
#include <string>
#include <vector>
#include <stdio.h>

namespace pyfree
{
	/**
	 * 向指定文件写入数据
	 * @param ptr {const char*} 待写入内容
	 * @param path {string} 文件名
	 * @param mode {string} 写入模式,等同于fopen或open函数的写入模式
	 * @return {bool} 
	 */
	bool writeToFile(const char *ptr,const std::string& path, const std::string& mode);
	/**
	 * 向指定文件追写数据
	 * @param list {vector} 待写入内容容器,每项作为一行写入
	 * @param path {string} 文件名
	 * @return {bool} 
	 */
	bool writeListInfo(std::vector<std::string> list,const std::string path);
	/**
	 * 从指定文件读取数据
	 * @param ptr {char*} 待读取内容指针
	 * @param size {int&} 实际读取大小
	 * @param buf_size {int} 待读取指针存储空间大小
	 * @param list {vector} 待写入内容容器,每项作为一行写入
	 * @param path {string} 文件名
	 * @param mode {string} 读取模式,等同于fopen或open函数的读取模式
	 * @return {bool} 
	 */
	bool readFromFile(char *ptr, int &size, int buf_size,const std::string& path, const std::string& mode);
	/**
	 * 从指定文件中读取文件每一行的字符串，如果行中存在空格，划分该行字符串，并装在到容器中
	 * @param list {vector<vector>} 读取内容容器,每一行作为一项载入容器,每行内容分割后容器装载
	 * @param path {string} 文件名
	 * @param codef {int} 读取时编码转换{1:g2u,2:a2u,其他不做处理}
	 * @return {bool} 
	 */
	bool readListInfo(std::vector<std::vector<std::string> > &list,const std::string path,int codef=0);
	/**
	 * 从指定文件中读取文件每一行的字符串，如果行中存在","间隔，划分该行字符串，并装在到容器中
	 * @param list {vector<vector>} 读取内容容器,每一行作为一项载入容器,每行内容分割后容器装载
	 * @param path {string} 文件名
	 * @param codef {int} 读取时编码转换{1:g2u,2:a2u,其他不做处理}
	 * @return {bool} 
	 */
	bool readListInfo_dot(std::vector<std::vector<std::string> > &list,const std::string path,int codef=0);
	/**
	 * 从指定文件中读取文件每一行的字符串并装在到容器中
	 * @param list {vector} 读取内容容器,每一行作为一项载入容器
	 * @param path {string} 文件名
	 * @param codef {int} 读取时编码转换{1:g2u,2:a2u,其他不做处理}
	 * @return {bool} 
	 */
	bool readListInfo(std::vector<std::string> &list,const std::string path,int codef=0);
	/**
	 * 将采用","间隔的字符串划分成子字符串集,被readListInfo_dot调用
	 * @param ids {vector} 读取内容容器
	 * @param idsStr {string} 被检查字符串
	 * @return {void} 
	 */
	void getNumberFromStr(std::vector<std::string> &ids, std::string idsStr);
	/**
	 * 从指定文件目录下读取指定扩展名的文件名,返回结果包含扩展名
	 * @param directory {string} 文件目录
	 * @param extName {string} 扩展名
	 * @param fileNames {vector} 读取结果
	 * @return {bool} 是否成功
	 */
	void getAllFileName_dot(const std::string& directory, const std::string& extName, std::vector<std::string>& fileNames);
	/**
	 * 从指定文件目录下读取指定扩展名的文件名,返回结果不包含扩展名
	 * @param directory {string} 文件目录
	 * @param extName {string} 扩展名
	 * @param fileNames {vector} 读取结果
	 * @return {bool} 是否成功
	 */
	void getAllFileName(const std::string& directory, const std::string& extName, std::vector<std::string>& fileNames);
	/**
	 * 从指定文件目录下读取指定扩展名的文件名,被getAllFileName_dot和getAllFileName调用
	 * @param directory {string} 文件目录
	 * @param extName {string} 扩展名
	 * @param fileNames {vector} 读取结果
	 * @param f {bool} 是否包含扩展名
	 * @return {bool} 是否成功
	 */
	void getAllFileNamePrivate(const std::string& directory
		, const std::string& extName
		, std::vector<std::string>& fileNames
		, bool f=false);
	/**
	 * 读取文件修改时间
	 * @param file_path {string} 文件名(全路径)
	 * @return {uint} 时间(time_t)
	 */
	unsigned int getFileModifyTime(const std::string file_path);
	/**
	 * 重命名文件
	 * @param _oldName {string} 旧文件名
	 * @param _newName {string} 新文件名
	 * @return {void}
	 */
	void renameFile(const std::string& _oldName, const std::string& _newName);
	/**
	 * 删除文件
	 * @param _Name {string} 文件名(全路径)
	 * @return {void}
	 */
	void removeFile(const std::string& _Name);
	/**
	 * 文件是否存在
	 * @param file {string} 文件名(全路径)
	 * @return {bool}
	 */
	bool isExist(std::string file);
	/**
	 * 文件在指定目录下是否存在
	 * @param dir {string} 指定目录
	 * @param file {string}
	 * @return {bool}
	 */
	bool isExist(std::string dir,std::string file);
	/**
	 * 将多很节点的xml表述文件重新批量重写为单根节点的xml文件集
	 * @param _xmlFile {string} 文件名
	 * @param _xmlDir {string} 目录
	 * @param _xmlDiv {string} 根节点名
	 * @param utfFlag {bool} 是否utf编码
	 * @return {void}
	 */
	void divXmlFile(std::string _xmlFile,std::string _xmlDir,std::string _xmlDiv,bool utfFlag);
	/**
	 * 创建文件目录
	 * @param _dir {string} 目录名
	 * @return {bool}
	 */
	bool createDir(std::string _dir);
	/**
	 * 文件目录是否存在
	 * @param _dir {string} 目录名
	 * @return {bool}
	 */
	bool isExistDir(std::string _dir);
	/**
	 * 获取程序当前所在目录
	 * @param dir_ {char*} 目录名
	 * @return {bool}
	 */
	bool getCurrentDir(char* dir_, int len_);
};

#endif //_FILE_H_
